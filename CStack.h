#pragma once

template<class T>
class CStack
{
private:
	int mSize;
	T *mData;
public:
	CStack();
	~CStack();
	
	//Return index of inserted value
	int Push(T mValue);
	//Return last value
	T Pop();
};

template<class T>
CStack<T>::CStack()
{
	this->mSize = 0;
	this->mData = nullptr;
}

template<class T>
CStack<T>::~CStack()
{
	if (this->mData) {
		delete[] this->mData;
	}
}

template<class T>
int CStack<T>::Push(T mValue)
{
	this->mSize++;
	if (!this->mData) {
		this->mData = new T[this->mSize];
	}
	else {
		T* mTemp = new T[this->mSize - 1];
		memcpy(mTemp, this->mData, sizeof(this->mData));
		delete[] this->mData;
		this->mData = new T[this->mSize];
		memcpy(this->mData, mTemp, sizeof(mTemp));
		delete[] mTemp;
	}
	this->mData[this->mSize - 1] = mValue;

	return this->mSize - 1;
}

template<class T>
T CStack<T>::Pop()
{
	if (!this->mData) {
		return NULL;
	}
	this->mSize--;

	T mResult = this->mData[this->mSize];
	if (this->mSize) {
		T* mTemp = new T[this->mSize];
		memcpy(mTemp, this->mData, sizeof(this->mData) - sizeof(T));
		delete[] this->mData;
		this->mData = new T[this->mSize];
		memcpy(this->mData, mTemp, sizeof(mTemp));
		delete[] mTemp;
	}
	else {
		delete[] this->mData;
		this->mData = nullptr;
	}
	

	return mResult;
}