#include <iostream>
#include "CStack.h"

int main()
{
    CStack<int> mStack;
    mStack.Push(1);
    mStack.Push(2);
    
    printf("%d\n", mStack.Pop());
    printf("%d\n", mStack.Pop());
    printf("%d\n", mStack.Pop());
}
